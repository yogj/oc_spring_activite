package org.example.demo.ticket.batch;


import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.example.demo.ticket.business.ManagerFactoryImpl;
import org.example.demo.ticket.model.bean.ticket.TicketStatut;
import org.example.demo.ticket.model.exception.TechnicalException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;




/**
 * Classe Principale de lancement des Batches.
 *
 * @author lgu
 */
public class Main {

    /** Logger pour la classe */
    private static final Log LOGGER = LogFactory.getLog(Main.class);
	private static Properties prop = new Properties ();
	private static File config = new File("data/conf/config.properties");


    /**
     * The entry point of application.
     *
     * @param pArgs the input arguments
     * @throws TechnicalException sur erreur technique
     */
    public static void main(String[] pArgs) throws TechnicalException {
        ApplicationContext vAppCon = new ClassPathXmlApplicationContext("classpath:/applicationContext.xml");
        // Il est possible de récupérer un bean dans ce contexte :
        ManagerFactoryImpl vManagerFactory = vAppCon.getBean("managerFactory", ManagerFactoryImpl.class);
    	
    	
    	try {
            if (pArgs.length < 1) {
                throw new TechnicalException("Veuillez préciser le traitement à effectuer !");
            }

            String vTraitementId = pArgs[0];
            if ("ExportTicketStatus".equals(vTraitementId)) {
                LOGGER.info("Execution du traitement : ExportTicketStatus");
                // ...creation du fichier txt
                ecrireFichier();
            } else {
                throw new TechnicalException("Traitement inconnu : " + vTraitementId);
            }
        } catch (Throwable vThrowable) {
            LOGGER.error(vThrowable);
            System.exit(1);
        }
    }
    
    private static void ecrireFichier() {
    	ObjectInputStream ois;
    	File fichier;
    	ArrayList<TicketStatut> list = new ArrayList<TicketStatut>();
    	//--on commence par lire le fichier properties pour recuperer le chemin du repertoire qui herbergera la fichier
    	try {
			if (config.exists()) {
				if (config.length() !=0) {
					ois = new ObjectInputStream(
								new BufferedInputStream(
										new FileInputStream(config)));
					prop.load(ois);
					//System.out.println("list proprietes recup sur le fichier lors de lecture : \n"+this.prop.toString());//Controle
					LOGGER.info("list proprietes recup sur le fichier lors de lecture : \n"+prop.toString());
					ois.close();
				}
			}
			//--Sinon, on affiche l'erreur
			else {
				LOGGER.fatal("le fichier config.properties n'existe pas");
			}
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}
    	//--creation du fichier vide
    	String chemin = String.valueOf(prop.getProperty("chemin"));
    	fichier = new File(chemin);
			
		try {
            fichier.createNewFile();
    	}catch (IOException e) {
    	    e.printStackTrace();
    	}

		//--on recupere les donnees a ecrire dans la bdd
		//--traitement pour recuperer un contenu pour list

    			
    			
    	//--on ecrit dans le fichier
    	try {
    		BufferedWriter fichTxT = new BufferedWriter(new FileWriter(fichier));
    		
    		for (TicketStatut ts : list ) {//--liste des ticketsStatuts issus de la bdd
    			fichTxT.write(ts.toString());
    			fichTxT.newLine();
    		}
    		
    		fichTxT.close();
    		LOGGER.debug("Taille du fichier = "+fichier.getName()+" " + fichier.length()); //Controle
    	}catch (FileNotFoundException e) {
    		e.printStackTrace();
    	}catch (IOException e) {
    		e.printStackTrace();				
    	}
    		
    }
}
