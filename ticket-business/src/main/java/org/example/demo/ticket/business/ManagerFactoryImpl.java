package org.example.demo.ticket.business;

import javax.inject.Inject;
import javax.inject.Named;

import org.example.demo.ticket.business.managerImpl.*;

@Named("managerFactory")
public class ManagerFactoryImpl implements ManagerFactory {
	
	@Inject
	private ProjetManagerImpl pm;
	@Inject
	private TicketManagerImpl tm;
	
    public ProjetManagerImpl getProjetManager() {
    	return pm;
    }
    // Ajout d'un setter pour l'attribut projetManager
    
    public void setProjetManager(ProjetManagerImpl pProjetManager) {
        pm = pProjetManager;
    }

    
    public TicketManagerImpl getTicketManager() {
        return tm;
    }
    // Ajout d'un setter pour l'attribut projetManager
    
    public void setTicketManager(TicketManagerImpl pTicketManager) {
        tm = pTicketManager;
    }
    
}
