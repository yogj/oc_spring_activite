package org.example.demo.ticket.business;

import org.example.demo.ticket.business.managerImpl.ProjetManagerImpl;
import org.example.demo.ticket.business.managerImpl.TicketManagerImpl;

public interface ManagerFactory {

    public ProjetManagerImpl getProjetManager() ;
    
    // Ajout d'un setter pour l'attribut projetManager
    public void setProjetManager(ProjetManagerImpl pProjetManager) ;
    
    public TicketManagerImpl getTicketManager() ;
    // Ajout d'un setter pour l'attribut projetManager
    public void setTicketManager(TicketManagerImpl pTicketManager) ;
}
