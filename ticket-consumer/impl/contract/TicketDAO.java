package contract;

import java.util.List;

import org.example.demo.ticket.model.bean.ticket.TicketStatut;
import org.example.demo.ticket.model.recherche.ticket.RechercheTicket;

/**
 * Interface DAO du package
 * {@link org.example.demo.ticket.model.bean.ticket}
 */
public interface TicketDAO {

	int getCountTicket(RechercheTicket pRechercheTicket);

	List<TicketStatut> getListStatut();

}
