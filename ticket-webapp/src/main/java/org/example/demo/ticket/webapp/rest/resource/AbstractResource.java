package org.example.demo.ticket.webapp.rest.resource;



import javax.inject.Inject;

import org.example.demo.ticket.business.ManagerFactoryImpl;

public abstract class AbstractResource {
	@Inject
	private static ManagerFactoryImpl managerFactory;


    protected static ManagerFactoryImpl getManagerFactory() {
        return managerFactory;
    }

    public static void setManagerFactory(ManagerFactoryImpl pManagerFactory) {
        managerFactory = pManagerFactory;
    }
}
