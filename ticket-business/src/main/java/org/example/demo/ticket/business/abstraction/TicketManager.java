package org.example.demo.ticket.business.abstraction;

import org.example.demo.ticket.model.bean.ticket.Commentaire;
import org.example.demo.ticket.model.bean.ticket.Ticket;
import org.example.demo.ticket.model.bean.ticket.TicketStatut;
import org.example.demo.ticket.model.bean.utilisateur.Utilisateur;
import org.example.demo.ticket.model.exception.NotFoundException;

public interface TicketManager {
	Ticket getTicket(Long pNumero) throws NotFoundException;

	void changerStatut(Ticket pTicket, TicketStatut pNewStatut, Utilisateur pUtilisateur, Commentaire pCommentaire);
}
